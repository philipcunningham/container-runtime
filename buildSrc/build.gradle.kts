repositories {
    mavenCentral()
    mavenLocal()
}

plugins {
    kotlin("jvm") version "1.9.24"
    kotlin("plugin.allopen") version "1.9.24"
}

val dockerJavaVersion = "3.3.6"

dependencies {
    // Docker dependencies
    implementation("com.github.docker-java:docker-java:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-httpclient5:$dockerJavaVersion")
}
