package nl.rug.digitallab.themis.runtime.container.buildsrc.runintegration

import com.github.dockerjava.api.model.Bind
import com.github.dockerjava.api.model.Volume
import nl.rug.digitallab.themis.runtime.container.buildsrc.GenericCallback
import nl.rug.digitallab.themis.runtime.container.buildsrc.TaskConfig
import nl.rug.digitallab.themis.runtime.container.buildsrc.createDockerClient
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.util.*

/**
 * A task for running the image used in integration testing.
 * The container needs privileged access for Docker in Docker to work.
 *
 * The task should also make sure that the output of the container is printed to the Gradle console.
 * This involves both logging, and waiting to make sure the task doesn't end early.
 *
 * This task will always succeed, even if a test fails.
 */
abstract class RunIntegrationTask : DefaultTask() {
    // The compiled container runtime
    @InputDirectory
    lateinit var runtimeBuild: File

    // The compiled integration tester
    @InputDirectory
    lateinit var testerBuild: File

    // The directory hosting the integration tests
    @InputDirectory
    lateinit var tests: File

    /**
     * Runs the image used in integration testing.
     */
    @TaskAction
    fun runEnvironment() {
        val containerId = "container-integration-" + UUID.randomUUID().toString()

        val dockerClient = createDockerClient()

        val createCmd = dockerClient.createContainerCmd(TaskConfig.IMAGE_NAME)
            .withName(containerId)

        val hostConfig = createCmd.hostConfig ?: throw Exception("Host config is null")

        createCmd.withHostConfig(
            hostConfig
                .withPrivileged(true)
                .withBinds(
                    Bind(runtimeBuild.path, Volume("/integration/container-runtime")),
                    Bind(testerBuild.path, Volume("/integration/integration-tester")),
                    Bind(tests.path, Volume("/integration/tests")),
                )
                .withAutoRemove(true)
        )

        createCmd.exec()

        dockerClient.startContainerCmd(containerId)
            .exec()

        dockerClient.logContainerCmd(containerId)
            .withStdOut(true)
            .withStdErr(true)
            .withFollowStream(true)
            .withTailAll()
            .exec(GenericCallback(logger))

        dockerClient.waitContainerCmd(containerId)
            .exec(GenericCallback(logger))
            .awaitCompletion()
    }
}
