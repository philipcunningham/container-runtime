package nl.rug.digitallab.themis.runtime.container.buildsrc.buildintegration

import nl.rug.digitallab.themis.runtime.container.buildsrc.GenericCallback
import nl.rug.digitallab.themis.runtime.container.buildsrc.TaskConfig
import nl.rug.digitallab.themis.runtime.container.buildsrc.createDockerClient
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * A task for building the image used in integration testing.
 * Its only responsibility is to call the relevant Dockerfile.
 * The specifics of the contents of the integration environment is up to the Dockerfile.
 */
abstract class BuildIntegrationTask : DefaultTask() {
    @InputFile
    lateinit var dockerfile: File

    @InputDirectory
    lateinit var srcDir: File

    /**
     * Builds the image used in integration testing.
     */
    @TaskAction
    fun buildEnvironment() {
        val dockerClient = createDockerClient()

        logger.info("Building environment image...")

        // Build the image in the host's docker daemon
        dockerClient.buildImageCmd()
            .withTags(setOf(TaskConfig.IMAGE_NAME))
            .withBaseDirectory(srcDir)
            .withDockerfile(dockerfile)
            .exec(
                GenericCallback(logger)
            ).awaitCompletion()
    }
}