package nl.rug.digitallab.themis.runtime.container.buildsrc

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient
import com.github.dockerjava.transport.DockerHttpClient
import java.time.Duration

/**
 * Creates a docker-java Docker client.
 *
 * @return The Docker client.
 */
fun createDockerClient(): DockerClient {
    // Config for the Docker client
    val config: DockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder().build()

    // The actual object used to make HTTP requests to docker
    val httpClient: DockerHttpClient = ApacheDockerHttpClient.Builder()
        .dockerHost(config.dockerHost)
        .sslConfig(config.sslConfig)
        .maxConnections(100)
        .connectionTimeout(Duration.ofSeconds(30))
        .responseTimeout(Duration.ofSeconds(45))
        .build()

    return DockerClientImpl.getInstance(config, httpClient)
}