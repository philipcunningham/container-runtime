package nl.rug.digitallab.themis.runtime.container.buildsrc

import com.github.dockerjava.api.async.ResultCallbackTemplate
import com.github.dockerjava.api.model.BuildResponseItem
import com.github.dockerjava.api.model.Frame
import org.gradle.api.logging.Logger

/**
 * A callback to process the output of generic docker commands.
 *
 * @property T The type of the response.
 */
open class GenericCallback<T>(
    private val logger: Logger
) : ResultCallbackTemplate<GenericCallback<T>, T>() {
    /**
     * Called whenever a message is received.
     */
    override fun onNext(response: T?) {
        when (response) {
            is BuildResponseItem ->
                if (response.isErrorIndicated) {
                    logger.error("Error: ${response.errorDetail?.message}")
                } else if (response.stream != null) {
                    print(response.stream)
                }
            is Frame -> print(String(response.payload, Charsets.UTF_8))
        }
    }

    /**
     * Called whenever an error occurs.
     */
    override fun onError(throwable: Throwable?) {
        throw throwable!!
    }
}