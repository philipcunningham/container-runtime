package nl.rug.digitallab.themis.runtime.container.buildsrc

object TaskConfig {
    const val IMAGE_NAME = "container-integration-environment:latest"
}
