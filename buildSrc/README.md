# Build source
This project contains code used for custom Gradle tasks and plugins.

# Tasks
The source code currently makes the following tasks available:
- tarImage
- runIntegration
- buildIntegration

It is up to the individual build.gradle.kts files to actually add the tasks to the list of available gradle tasks.