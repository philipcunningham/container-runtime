import nl.rug.digitallab.themis.runtime.container.buildsrc.buildintegration.BuildIntegrationTask
import nl.rug.digitallab.themis.runtime.container.buildsrc.runintegration.RunIntegrationTask

val projectGroupId: String by project

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

subprojects {
    group = projectGroupId

    apply {
        plugin("nl.rug.digitallab.gradle.plugin.quarkus.project")
    }
}

tasks.register<Copy>("prepareIntegrationBuilding") {
    group = "digital lab"
    description = "Copies the necessary files to the build directory"
    from(
        "integration-tester/environment/Dockerfile",
        "integration-tester/environment/entrypoint.sh",
        "integration-tester/environment/compose.yaml",
        "integration-tester/environment/runsc-docker.json",
        "integration-tester/environment/monitor-syscalls.json",
    )
    into("container-runtime/build/docker")
}

// Register the custom gradle tasks
tasks.register<BuildIntegrationTask>("buildIntegration") {
    group = "digital lab"
    description = "Builds the docker image used for integration testing"
    dependsOn(
        "prepareIntegrationBuilding",
        "container-runtime:build",
        "integration-tester:build",
    )

    dockerfile = File(project(":container-runtime").layout.buildDirectory.asFile.get().path + "/docker/Dockerfile")
    srcDir = File(project(":container-runtime").layout.buildDirectory.asFile.get().path + "/docker")
}

tasks.register<RunIntegrationTask>("runIntegration") {
    group = "digital lab"
    description = "Runs the docker image to perform integration testing"
    dependsOn("buildIntegration")

    runtimeBuild = File(project(":container-runtime").layout.buildDirectory.asFile.get().path + "/quarkus-app")
    testerBuild = File(project(":integration-tester").layout.buildDirectory.asFile.get().path + "/quarkus-app")
    tests = File(project(":integration-tester").layout.projectDirectory.asFile.path + "/tests")
}
