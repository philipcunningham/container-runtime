# Container Runtime
This repository contains the source code for the container runtime, a component within the Themis project.

Themis is the university of Groningen's system for automating the grading of student's code submissions.

The container runtime is responsible for executing the high-level commands sent by the runtime orchestrator. These commands describe virtualised workloads that should be run. The container runtime then takes care of tasks like secure virtualisation, resource limits, and result monitoring.

## Authors
### Organisation
University of Groningen

### Development team
Digital lab

### Current maintainer
Lars Andringa

## Deployment requirements
### Infrastructure
- XFS filesystem backing docker's storage drivers
- Any modern Linux OS

### Packages
- Docker
- gVisor (runsc)

### Configuration
- Docker daemon's configuration should match `/config/runsc-docker.json`
- The container runtime should run as a privileged docker container

## Supported features
### Request
- [x] Resource limits
  - [x] Walltime
  - [x] CPU cores (fractional)
  - [x] Memory (Currently does not account for memory used by gVisor)
  - [x] Disk space
  - [x] Container processes
  - [x] User processes
  - [x] Output stream
- [x] Environment
- [x] Startup command
- [x] Environment variables
- [x] Input files
- [x] Requested output files
- [x] Stdin
- [x] Features
  - [x] Trust level

### Response
- [x] Exit code
- [ ] Status
  - [x] SUCCEEDED
  - [x] CRASHED
  - [x] EXCEEDED_WALL_TIME
  - [x] EXCEEDED_MEMORY
  - [x] EXCEEDED_STREAM_OUTPUT
  - [X] EXCEEDED_OUTPUT_FILE
  - [x] EXCEEDED_PROCESSES (Only for user processes exceeded)
  - [ ] PRODUCED_ORPHANS
  - [ ] PRODUCED_ZOMBIES
  - [ ] PRODUCED_ZOMBIES_AND_ORPHANS
- [x] Stdout stream
- [x] Stderr stream
- [x] Output files
- [ ] Resource usage
  - [x] Walltime
  - [ ] Memory
  - [x] Disk space
  - [x] Output stream
  - [x] Created processes
  - [x] Max processes
  - [ ] Orphaned processes
  - [ ] Zombie processes
  - [ ] IO time
  - [ ] CPU time

Note that code depending on the protobuf spec will error out until you've built for the first time.
This is because during the build process, the protospec is turned into Java files that can actually be included in the codebase.

## Containerisation
### Building the container
The container runtime can be built using the Dockerfile available at:
```
/container-runtime/src/main/docker/Dockerfile.jvm
```

Note that the build context for `docker build` should be at `/container-runtime`

The following command can be used from the build context:
```
docker build -t container-runtime ./container-runtime -f ./container-runtime/src/main/docker/Dockerfile.jvm
```

### Running the container
The container runtime can be run using the following docker command:
```
docker run -v /var/run/docker.sock:/var/run/docker.sock -i -p 8080:8080 --user root {Image}
```

### Building the integration tester
The integration tester can be built using the Dockerfile available at:
```
/integration-tester/src/main/docker/Dockerfile.jvm
```

Note that the build context for `docker build` should be at `/integration-tester`

The following command can be used from the build context:
```
docker build -t integration-tester ./integration-tester -f ./integration-tester/src/main/docker/Dockerfile.jvm
```
