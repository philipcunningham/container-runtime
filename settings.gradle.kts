rootProject.name = "container-runtime"

pluginManagement {
    val digitalLabGradlePluginVersion: String by settings
    val jandexPluginVersion: String by settings

    plugins {
        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
        id("org.kordamp.gradle.jandex") version jandexPluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}

include("container-runtime")
include("integration-tester")
include("container-runtime:process-monitor")
include("container-runtime:container-runner")
include("container-runtime:runtime-application")
findProject(":container-runtime:runtime-application")?.name = "runtime-application"
