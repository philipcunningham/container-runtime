# This script is the entrypoint for the testing environment.
# It will load and spin up the container runtime and integration tester.

# Finish mounting the XFS filesystem with privileged access
mount -o prjquota -t xfs xfs.bin /var/lib/docker

# Start the docker daemon with the gVisor runtime
dockerd --add-runtime runsc=/usr/local/bin/runsc &

# Wait for the docker daemon to have started
while (! docker stats --no-stream ); do
  sleep 1
done

# Start the integration testing environment
docker load -i /base-image.tar

docker compose --project-name integration up --abort-on-container-exit
