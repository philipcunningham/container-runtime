val jacksonProtobufVersion: String by rootProject
val communicationSpecVersion: String by rootProject

dependencies {
    implementation("io.quarkus:quarkus-jackson")

    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.hubspot.jackson:jackson-datatype-protobuf:$jacksonProtobufVersion")

    implementation("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")
}

