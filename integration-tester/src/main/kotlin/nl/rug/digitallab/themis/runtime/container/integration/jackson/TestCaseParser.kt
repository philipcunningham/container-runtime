package nl.rug.digitallab.themis.runtime.container.integration.jackson

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeType
import com.fasterxml.jackson.databind.node.LongNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.exceptions.ByteSizeFormatException
import java.nio.file.Path
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.io.path.exists
import kotlin.io.path.readBytes

class TestCaseParser(private val directory: Path) {
    fun walkTree(json: JsonNode): JsonNode {
        return when(json.nodeType) {
            JsonNodeType.STRING -> visitString(json)
            JsonNodeType.OBJECT -> visitObject(json.withObject(""))
            JsonNodeType.ARRAY -> visitArray(json.withArray(""))
            else -> json
        }
    }

    @OptIn(ExperimentalEncodingApi::class)
    private fun processFileReference(content: String): String {
        val filename = content.removePrefix("@")
        val file = directory.resolve(filename)
        require(file.exists()) { "File $filename does not exist" }
        return Base64.encode(file.readBytes())
    }

    /**
     * Visits each string in the testcase yaml and processes them
     * This processing is used to implement new features into the test specification not supported by a simple direct mapping
     *
     * Supports:
     * - File references, allows you to externally write content so that you don't have to put them directly in the yaml
     * - ByteSize library, allows you to specify sizes in a more human-readable format
     */
    private fun visitString(json: JsonNode): JsonNode {
        val content = json.asText()

        // File reference, load in the file content into the field
        if (content.startsWith("@"))
            return TextNode(processFileReference(content))

        // Strings specified using the ByteSize library, convert them to raw bytes as a long
        try {
            ByteSize.fromString(content)
            return LongNode(ByteSize.fromString(content).B)
        } catch (e: ByteSizeFormatException) {
            // Not a ByteSize, ignore
        }

        // No special string, just return the node
        return json
    }

    private fun visitObject(json: ObjectNode): JsonNode {
        json.fieldNames().forEach { json.replace(it, walkTree(json[it])) }
        return json
    }

    private fun visitArray(json: ArrayNode): JsonNode {
        for (i in 0..<json.size())
            json[i] = walkTree(json[i])

        return json
    }
}
