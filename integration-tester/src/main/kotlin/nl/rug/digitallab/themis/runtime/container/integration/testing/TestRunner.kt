package nl.rug.digitallab.themis.runtime.container.integration.testing

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.container.integration.io.GrpcClient
import nl.rug.digitallab.themis.runtime.container.integration.io.Testcase
import nl.rug.digitallab.themis.runtime.container.integration.jackson.TestCaseParser
import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.Trust
import nl.rug.protospec.judgement.v1.features
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.jboss.logging.Logger
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.math.abs
import kotlin.system.exitProcess

/**
 * The test runner for running all tests.
 */
@ApplicationScoped
class TestRunner {
    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var grpcClient: GrpcClient

    @Inject
    private lateinit var mapper: YAMLMapper

    @ConfigProperty(name = "tests.dir")
    private lateinit var testsDir: String

    private var testCounter = 0

    /**
     * Runs all tests.
     */
    fun runTests() {
        val testsRoot = Path(testsDir)

        testsRoot
            .listDirectoryEntries()
            .map { it.resolve("testcase.yaml") }
            .filter { it.exists() }
            .map {
                var specTree = mapper.readTree(it.toFile())
                specTree = TestCaseParser(it.parent).walkTree(specTree)

                val spec = mapper.treeToValue(specTree, Testcase::class.java)
                return@map spec.copy(name = it.parent.name)
            }
            .forEach(::runTestsForAllRuntimes)
    }

    /**
     * For every integration test, run the test for the various runtimes.
     * Every test will currently run with the default docker runtime, and another time with the gVisor runtime.
     *
     * @param baseCase The test specification
     */
    private fun runTestsForAllRuntimes(baseCase: Testcase) {
        for (runtime in baseCase.runtimes) {
            // Create a test for the specific runtime currently being tested
            val dockerRuntimeTestcase = baseCase.copy(
                input = baseCase.input.toBuilder().setFeatures(
                    features {
                        isTrusted = Trust.valueOf(runtime)
                    }
                ).build(),
                name = "${baseCase.name}-$runtime",
            )

            // Run the test
            runTest(dockerRuntimeTestcase, Trust.valueOf(runtime))
        }
    }

    /**
     * Runs a single test.
     *
     * @param testcase The test specification
     * @param runtime The runtime used to run the test
     */
    private fun runTest(testcase: Testcase, runtime: Trust) {
        testCounter++
        logger.info("Running test ${testCounter}: ${testcase.name}")

        try {
            val response = grpcClient.callRuntime(testcase.input)

            if (testcase.thrownException != null) {
                logger.error("Test $testCounter (${testcase.name}) failed: no exception thrown")
                exitProcess(1)
            }

            compareResults(response, testcase, runtime)
        } catch(e: Exception) {
            if (testcase.thrownException == e.javaClass.simpleName) {
                logger.info("Test $testCounter (${testcase.name}) passed")
                return
            }

            logger.error("Test $testCounter (${testcase.name}) failed: exception thrown")
            logger.error("Stacktrace:" + System.lineSeparator() + e.stackTraceToString())

            exitProcess(1)
        }

        logger.info("Test $testCounter (${testcase.name}) passed")
    }

    /**
     * Asserts that the actual value is within the expected value, with a margin.
     * The program will exit if the assertion fails.
     *
     * @param actual The actual value
     * @param expected The expected value
     * @param margin The maximum margin between the actual and expected value
     * @param name The name of the test, to be used for logging.
     */
    private fun <T : Number> assertWithinLimit(actual: T, expected: T, margin: T, name: String, resourceName: String) {
        val actualLong = actual.toLong()
        val expectedLong = expected.toLong()
        val marginLong = margin.toLong()

        if (abs(actualLong - expectedLong) > marginLong) {
            logger.error("Test $testCounter ($name) failed: $resourceName was not within the margin of $marginLong")
            logger.error("Expected: $expectedLong")
            logger.error("Actual: $actualLong")

            exitProcess(1)
        }
    }

    /**
     * Compares the actual response to the expected response to check if the test passed.
     *
     * @param actual The actual response from the judgement manager.
     * @param testcase The test specification
     * @param runtime The runtime used to run the test
     */
    private fun compareResults(actual: RunActionResponse, testcase: Testcase, runtime: Trust) {
        if (testcase.name == null) {
            logger.error("Test $testCounter failed: no name specified")
            exitProcess(1)
        }

        if (actual.result != testcase.expectedOutput.result) {
            logger.error("Test $testCounter (${testcase.name}) failed: response does not match expected response")
            logger.error("Expected:${System.lineSeparator()}${testcase.expectedOutput}")
            logger.error("Actual:${System.lineSeparator()}$actual")
            // This is unfortunately the way protobuf handles this, the only alternative I have found so far is to manually print all fields
            logger.error("Note that only fields are shown that are not set to their default value.")

            exitProcess(1)
        }

        assertWithinLimit(actual.usage.wallTimeMs, testcase.expectedOutput.usage.wallTimeMs, testcase.usageMargin.wallTimeMs, testcase.name, "WallTimeMs")
        assertWithinLimit(actual.usage.memoryBytes, testcase.expectedOutput.usage.memoryBytes, testcase.usageMargin.memoryBytes, testcase.name, "MemoryBytes")
        assertWithinLimit(actual.usage.diskBytes, testcase.expectedOutput.usage.diskBytes, testcase.usageMargin.diskBytes, testcase.name, "DiskBytes")
        assertWithinLimit(actual.usage.outputBytes, testcase.expectedOutput.usage.outputBytes, testcase.usageMargin.outputBytes, testcase.name, "OutputBytes")
        if (runtime == Trust.UNTRUSTED) {
            assertWithinLimit(actual.usage.createdProcesses, testcase.expectedOutput.usage.createdProcesses, testcase.usageMargin.createdProcesses, testcase.name, "CreatedProcesses")
            assertWithinLimit(actual.usage.maxProcesses, testcase.expectedOutput.usage.maxProcesses, testcase.usageMargin.maxProcesses, testcase.name, "MaxProcesses")
        }
    }
}
