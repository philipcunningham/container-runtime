package nl.rug.digitallab.themis.runtime.container.integration.io

import io.grpc.ManagedChannelBuilder
import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.protospec.judgement.v1.ActionRunnerGrpc
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse

/**
 * The configuration for the gRPC client.
 *
 * @property host The host of the gRPC server.
 * @property port The port of the gRPC server.
 */
@StaticInitSafe
@ConfigMapping(prefix = "grpc")
interface GrpcConfig {
    fun host(): String
    fun port(): Int
}

/**
 * The gRPC client for sending the RunAction request to the judgement manager.
 */
@ApplicationScoped
class GrpcClient {
    @Inject
    private lateinit var config: GrpcConfig

    /**
     * Sends the RunAction request to the judgement manager.
     *
     * @param request The request to send.
     *
     * @return The response from the judgement manager.
     */
    fun callRuntime(request: RunActionRequest): RunActionResponse {
        val channel = ManagedChannelBuilder.forAddress(config.host(), config.port())
            .usePlaintext()
            .build()

        try {
            val stub = ActionRunnerGrpc.newBlockingStub(channel)

            return stub.runAction(request)
        } finally {
            channel.shutdown()
        }
    }
}
