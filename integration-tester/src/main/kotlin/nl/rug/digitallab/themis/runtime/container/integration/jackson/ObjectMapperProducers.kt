package nl.rug.digitallab.themis.runtime.container.integration.jackson

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import io.quarkus.arc.All
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.enterprise.inject.Produces
import jakarta.inject.Singleton

/**
 * Factory class to produce custom ObjectMapper for YAML. This way,
 * we bypass the default Quarkus ObjectMapper, since that one only
 * accepts JSON and has a lot of custom configuration that we want
 * to manage ourselves.
 */
class ObjectMapperProducers {
    @Singleton
    @Produces
    fun yamlMapper(@All customizers: MutableList<ObjectMapperCustomizer>): YAMLMapper {
        val mapper = YAMLMapper()
        customizers.forEach { it.customize(mapper) }

        return mapper
    }
}
