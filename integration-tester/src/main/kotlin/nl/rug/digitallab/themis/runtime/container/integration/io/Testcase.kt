package nl.rug.digitallab.themis.runtime.container.integration.io

import nl.rug.protospec.judgement.v1.ResourceUsage
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse

/**
 * A testcase, containing the data for the test.
 *
 * @property name The name of the testcase.
 * @property runtimes The runtimes to run the testcase on.
 * @property input The request to send over gRPC to the container runtime.
 * @property thrownException The expected exception to be thrown by the container runtime.
 * null if no exception is expected.
 * @property expectedOutput The expected response from the container runtime.
 * @property usageMargin Resource usage cannot be exactly predicted, so this is the margin of error
 */
data class Testcase (
    val name: String?,
    val runtimes: List<String>,
    val input: RunActionRequest,
    val thrownException: String?,
    val expectedOutput: RunActionResponse,
    val usageMargin: ResourceUsage = ResourceUsage.getDefaultInstance(),
)
