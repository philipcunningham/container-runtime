val dockerJavaVersion: String by rootProject
val commonQuarkusTelemetry: String by rootProject
val commonHelpersVersion: String by rootProject
val otelKotlinVersion: String by rootProject
val communicationSpecVersion: String by rootProject

plugins {
    id("org.kordamp.gradle.jandex")
}

dependencies {
    // Docker dependencies
    implementation("com.github.docker-java:docker-java:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-httpclient5:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-netty:$dockerJavaVersion")

    // Digital Lab dependencies
    api("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusTelemetry")

    // Module dependencies
    implementation(project(":container-runtime:process-monitor"))

    // Miscellaneous dependencies
    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$otelKotlinVersion")
}
