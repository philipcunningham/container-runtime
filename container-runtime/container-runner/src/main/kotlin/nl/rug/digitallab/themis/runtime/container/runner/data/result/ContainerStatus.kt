package nl.rug.digitallab.themis.runtime.container.runner.data.result

import nl.rug.protospec.judgement.v1.ExecutionStatus

/**
 * An enum describing the outcome of an execution (success, failures, why it failed, etc.).
 */
enum class ContainerStatus {
    SUCCEEDED,
    CRASHED,

    EXCEEDED_WALL_TIME,
    EXCEEDED_MEMORY,
    EXCEEDED_STREAM_OUTPUT,
    EXCEEDED_OUTPUT_FILE,
    EXCEEDED_PROCESSES,

    PRODUCED_ORPHANS,
    PRODUCED_ZOMBIES,
    PRODUCED_ZOMBIES_AND_ORPHANS;

    /**
     * Convert this enum to a [ExecutionStatus] proto.
     */
    fun toProto(): ExecutionStatus {
        return ExecutionStatus.valueOf(this.name)
    }

    /**
     * Defines the priority of each possible container status.
     * The higher the value, the higher the priority.
     * The final status of a container is the status with the highest priority.
     *
     * @return The priority of the status.
     */
    fun toPriority(): Int =
        when(this) {
            SUCCEEDED -> 0
            PRODUCED_ORPHANS -> 1
            PRODUCED_ZOMBIES -> 2
            PRODUCED_ZOMBIES_AND_ORPHANS -> 3
            CRASHED -> 4
            EXCEEDED_OUTPUT_FILE -> 5
            EXCEEDED_STREAM_OUTPUT -> 6
            EXCEEDED_PROCESSES -> 7
            EXCEEDED_MEMORY -> 8
            EXCEEDED_WALL_TIME -> 9
        }
}
