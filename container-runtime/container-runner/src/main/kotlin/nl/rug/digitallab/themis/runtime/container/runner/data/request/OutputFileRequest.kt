package nl.rug.digitallab.themis.runtime.container.runner.data.request

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.maxSize

/**
 * A request for a file to retrieve from the container after execution.
 *
 * @param path The path of the file to retrieve.
 * @param maxSize The maximum size of the file to retrieve.
 */
data class OutputFileRequest(
    val path: String,
    val maxSize: ByteSize,
) {
    companion object {
        /**
         * Convert a [nl.rug.protospec.judgement.v1.OutputFile] to an [OutputFileRequest].
         *
         * @param outputFile The file to convert.
         *
         * @return The converted file.
         */
        fun fromProto(outputFile: nl.rug.protospec.judgement.v1.OutputFileRequest): OutputFileRequest = OutputFileRequest(
            path = outputFile.path,
            maxSize = outputFile.maxSize,
        )
    }
}
