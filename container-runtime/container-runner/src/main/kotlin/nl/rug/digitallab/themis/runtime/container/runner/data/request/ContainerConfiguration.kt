package nl.rug.digitallab.themis.runtime.container.runner.data.request

import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.Trust

/**
 * The items that should be inserted into the container.
 *
 * @param image The image that the container should be built on.
 * @param trusted Whether the container should be run in trusted mode.
 * @param resourceLimits The resource limits the container should be constrained by.
 * @param envVars The environment variables to insert into the container.
 * @param inputFiles The files to insert into the container.
 * @param command The command to run in the container. The command's arguments are split into separate strings.
 */
data class ContainerConfiguration(
    val image: String,
    val trusted: Boolean,
    val resourceLimits: ResourceLimits,
    val envVars: Map<String, String>,
    val inputFiles: List<ContainerFile>,
    val outputFileRequests: List<OutputFileRequest>,
    val command: List<String>,
) {
    companion object {
        /**
         * Convert a [RunActionRequest] to a [ContainerConfiguration].
         *
         * @param request The request to convert.
         * @param config The configuration containing the minimum values the limits must take
         *
         * @return The converted configuration.
         */
        fun fromProto(request: RunActionRequest, config: ResourceLimitsConfig) = ContainerConfiguration(
            image = request.environment,
            trusted = request.features.isTrusted == Trust.TRUSTED,
            resourceLimits = ResourceLimits.fromProto(request.limits, config),
            envVars = request.envVarsMap,
            inputFiles = request.inputFilesList.map { ContainerFile.fromProto(it) },
            outputFileRequests = request.outputFileRequestsList.map { OutputFileRequest.fromProto(it) },
            command = request.commandList,
        )
    }
}
