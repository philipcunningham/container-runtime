package nl.rug.digitallab.themis.runtime.container.runner.contexts

import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerStatus
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ResourceUsage
import org.jboss.logging.Logger
import kotlin.properties.Delegates

/**
 * A container context used by the main container runner. It stores data about the container currently running.
 *
 * The idea is that this class can be used to track commonly used data without needing to pass it by parameters.
 * As this class is attached as a Kotlin context receiver, it can be accessed from functions without needing to pass it as a parameter.
 *
 * @property containerId The ID of the container attached to this context. Can be used to access the container from the docker client.
 * @property logger The logger to use for logging messages.
 */
class ContainerContext(
    val containerId: String,
    val logger: Logger
) {
    lateinit var containerExecStatus: ContainerStatus
    var exitCode by Delegates.notNull<Int>()
    val resourceUsage = ResourceUsage()

    /**
     * Reports a status for the container.
     * The status will be marked if it has a higher priority than the current status.
     *
     * @param status The status to mark.
     */
    fun reportStatus(status: ContainerStatus) {
         if (
            !::containerExecStatus.isInitialized ||                 // No status has been set yet
            status.toPriority() > containerExecStatus.toPriority()  // The new status has a higher priority than the current status
             ) {
             containerExecStatus = status
             logger.info("Status $status was marked as the new status of the container.")
        } else {
            logger.info("Status $status was not marked as it has a lower priority than the current status: $containerExecStatus")
        }
    }

    /**
     * Reports the exit code of the container.
     *
     * @param exitCode The exit code of the container.
     */
    fun reportExitCode(exitCode: Int) {
        this.exitCode = exitCode
    }
}

