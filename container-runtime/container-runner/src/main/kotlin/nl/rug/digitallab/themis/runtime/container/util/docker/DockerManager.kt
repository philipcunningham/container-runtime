package nl.rug.digitallab.themis.runtime.container.util.docker

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.exception.NotFoundException
import com.github.dockerjava.api.model.PullResponseItem
import com.github.dockerjava.api.model.WaitResponse
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.*
import nl.rug.digitallab.themis.runtime.container.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ContainerConfiguration
import nl.rug.digitallab.themis.runtime.container.util.docker.callback.ContainerLogger
import nl.rug.digitallab.themis.runtime.container.util.docker.callback.GenericCallback
import nl.rug.digitallab.themis.runtime.container.util.docker.clients.nettyFactory
import org.jboss.logging.Logger
import java.io.InputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream

/**
 * A helper class for docker related functions.
 *
 * @param dispatcher The dispatcher to use for coroutines within the [DockerManager].
 */
@ApplicationScoped
class DockerManager(
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
) {
    @Inject
    private lateinit var dockerClient: DockerClient

    @Inject
    private lateinit var logger: Logger

    /**
     * Static function to check if a container exists.
     *
     * @param image The image to check for.
     * @return True if the container exists, false otherwise.
     */
    @WithSpan("CHECK whether image is available")
    fun imageExists(image: String): Boolean =
        try {
            dockerClient.inspectImageCmd(image).exec()
            true
        } catch(e: NotFoundException) {
            false
        }

    /**
     * Static function to run the docker kill command.
     *
     * @param containerId The ID of the container to kill.
     */
    fun killContainer(containerId: String) =
        dockerClient.killContainerCmd(containerId).exec()

    /**
     * Static function to run the docker log command.
     *
     * @param logger A callback to process the output of the log command.
     * @param containerId The ID of the container to log.
     */
    fun logContainer(logger: ContainerLogger, containerId: String): ContainerLogger =
        dockerClient.logContainerCmd(containerId)
            .withStdOut(true).withStdErr(true).withFollowStream(true)
            .exec(logger)

    /**
     * Static function to wait for a container to finish.
     *
     * @param containerId The ID of the container to wait for.
     */
    fun waitContainer(containerId: String) {
        dockerClient.waitContainerCmd(containerId).exec(GenericCallback<WaitResponse>()).awaitCompletion()
    }

    /**
     * Static function to pull a docker image from a remote repository.
     *
     * @param image The image to pull.
     */
    @WithSpan("PULL image")
    fun pullImage(image: String): GenericCallback<PullResponseItem> =
        dockerClient.pullImageCmd(image).exec(GenericCallback()).awaitCompletion()

    /**
     * Static function to delete a docker container from the docker daemon.
     *
     * @param containerId The ID of the container to delete.
     */
    @WithSpan("DELETE container")
    fun deleteContainer(containerId: String) =
        dockerClient.removeContainerCmd(containerId).exec()

    /**
     * Static function to inspect a container by its ID.
     *
     * @param containerId The ID of the container to inspect.
     *
     * @return The container's information.
     */
    fun inspectContainer(containerId: String) =
        dockerClient.inspectContainerCmd(containerId).withSize(true).exec()

    /**
     * Static function to attach a provided stdin to a container.
     * The function shall do nothing if stdin == null.
     *
     * This helper function uses the Netty docker-java transport, rather than the default Apache HTTP5 transport.
     * This is because Apache HTTP5 is broken for attach, as closing the stream does not cause the stdin in the container to close.
     *
     * @param stdin The stdin [ContainerFile] to attach to the container.
     * @param containerId The ID of the container to attach the stdin to.
     */
     fun attachStdIn(stdin: ContainerFile?, containerId: String) {
         if (stdin == null) {
             return
         }

        val inputStream = PipedInputStream()
        PipedOutputStream(inputStream).use {outputStream ->
            outputStream.write(stdin.content)
        }

        nettyFactory().createAttachContainerCmdExec().exec(
            dockerClient.attachContainerCmd(containerId)
                .withStdIn(inputStream)
                .withFollowStream(true),
            GenericCallback(),
        )
    }

    /**
     * Static function to copy a tar archive into a container.
     * Abstraction level over the Docker copy to introduce a suspending function with Main Dispatch.
     * This is to introduce coroutine support to the docker client's copy tar command.
     *
     * @param inputStream The input stream of the tar archive.
     * @param containerId The ID of the container to copy the tar archive into.
     */
    suspend fun copyArchiveIntoContainer(
        inputStream: PipedInputStream,
        containerId: String,
    ) = withContext(dispatcher) {
        dockerClient.copyArchiveToContainerCmd(containerId)
            .withTarInputStream(inputStream)
            // The name contains the full path, so extract from the root
            .withRemotePath("/")
            .exec()
    }

    /**
     * Static function to retrieve a tar archive from a container.
     *
     * @param outputFile The path of the file to retrieve from the container.
     * @param containerId The ID of the container to retrieve the file from.
     *
     * @return An input stream containing the tar archive.
     */
    fun copyArchiveFromContainer(outputFile: String, containerId: String): InputStream =
        dockerClient.copyArchiveFromContainerCmd(containerId, outputFile).exec()

    /**
     * This function creates a container managed by the container runtime.
     * The container is not yet started, only created.
     *
     * @param containerConfiguration The configuration of the container to be run.
     *
     * @return The container context tracking the created container. Used to track the container's state and ID.
     */
    @WithSpan("CREATE container")
    fun createContainer(containerConfiguration: ContainerConfiguration): ContainerContext {
        val cmd = dockerClient.createContainerCmd(containerConfiguration.image)

        // Set the command to be run in the container
        if (containerConfiguration.command.isNotEmpty()) {
            cmd.withCmd(containerConfiguration.command)
        }

        val hostConfig = cmd.hostConfig ?: throw IllegalStateException("Host config of a container-to-create is null")

        // Make the container runtime use gVisor if the container is not trusted
        if (!containerConfiguration.trusted) {
            hostConfig.withRuntime("runsc")
        }

        // Insert environment variables into the container
        cmd.withEnv(containerConfiguration.envVars.map { "${it.key}=${it.value}" })

        // Enable reading from the external stdin
        cmd.withStdinOpen(true)
            .withStdInOnce(true)

        val limits = containerConfiguration.resourceLimits
        cmd.withHostConfig(
            hostConfig
                // CPU limits
                .withCpuQuota(limits.cpuCores.toLong() * 1_000_000)
                // Memory limits
                .withMemory(limits.memoryLimit.B)
                .withMemorySwap(limits.memoryLimit.B)
                // Disk limits (Requires XFS file system)
                .withStorageOpt(mapOf("size" to "${limits.diskLimit.KB}k"))
                // Process limits
                .withPidsLimit((limits.containerProcesses + limits.internalProcessCount).toLong())
        )

        val containerId = cmd.exec().id
        logger.info("Created container $containerId")

        return ContainerContext(containerId, logger)
    }

    /**
     * This function starts a container managed by the container runtime.
     * The container with the associated containerId is expected to be created already.
     *
     * @param containerId The ID of the container to start.
     */
    @WithSpan("START container")
    fun startContainer(containerId: String) {
        dockerClient.startContainerCmd(containerId).exec()
        logger.info("Started container $containerId")
    }
}
