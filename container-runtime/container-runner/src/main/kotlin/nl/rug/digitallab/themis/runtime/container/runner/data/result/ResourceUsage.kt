package nl.rug.digitallab.themis.runtime.container.runner.data.result

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.disk
import nl.rug.protospec.judgement.v1.memory
import nl.rug.protospec.judgement.v1.output
import nl.rug.protospec.judgement.v1.resourceUsage

/**
 * The resources used by an execution.
 * This includes wall time, memory, disk, etc.
 */
class ResourceUsage{
    var wallTimeMs: Int? = null
    var memory: ByteSize? = null
    var disk: ByteSize? = null
    var output: ByteSize? = null
    var createdProcesses: Int? = null // Only available on gVisor
    var maxProcesses: Int? = null     // Only available on gVisor

    /**
     * Convert this object to its protobuf equivalent.
     *
     * @return The protobuf equivalent of this object.
     */
    fun toProto() = resourceUsage {
        this@ResourceUsage.wallTimeMs?.let { wallTimeMs = it }
        this@ResourceUsage.memory?.let { memory = it }
        this@ResourceUsage.disk?.let { disk = it }
        this@ResourceUsage.output?.let { output = it }
        this@ResourceUsage.createdProcesses?.let { createdProcesses = it }
        this@ResourceUsage.maxProcesses?.let { maxProcesses = it }
    }
}
