package nl.rug.digitallab.themis.runtime.container.runner.data.common

import nl.rug.protospec.judgement.v1.FileMetaData
import nl.rug.protospec.judgement.v1.fileMetaData

/**
 * The metadata of a file.
 *
 * @param uid The user id of the file.
 * @param gid The group id of the file.
 *
 * @param userPermissions The permissions of the user.
 * @param groupPermissions The permissions of the group.
 * @param othersPermissions The permissions of others.
 */
data class FileMetadata(
    val uid: Int,
    val gid: Int,
    val userPermissions: FilePermissions,
    val groupPermissions: FilePermissions,
    val othersPermissions: FilePermissions,
) {
    val posixPermissions: Int
        // The first 9 bits of the permission integer are used for the permissions.
        // The first 3 bits are for the user, the next 3 for the group, and the last 3 for others.
        get() =
            (userPermissions.bitmask shl 6) or
            (groupPermissions.bitmask shl 3) or
            (othersPermissions.bitmask shl 0)

    /**
     * [FileMetadata] constructor that sets its permission using a POSIX permission integer.
     *
     * @param uid The user id of the file.
     * @param gid The group id of the file.
     * @param posixPermissions The POSIX permissions of the file encoded as an integer.
     */
    constructor(
        uid: Int,
        gid: Int,
        posixPermissions: Int,
    ) : this(
        uid = uid,
        gid = gid,
        userPermissions = FilePermissions(posixPermissions shr 6),
        groupPermissions = FilePermissions(posixPermissions shr 3),
        othersPermissions = FilePermissions(posixPermissions shr 0),
    )

    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): FileMetaData = fileMetaData {
        uid = this@FileMetadata.uid
        gid = this@FileMetadata.gid
        userPermissions = this@FileMetadata.userPermissions.toProto()
        groupPermissions = this@FileMetadata.groupPermissions.toProto()
        othersPermissions = this@FileMetadata.othersPermissions.toProto()
    }

    companion object {
        /**
         * Convert a ProtoFileMetaData to a [FileMetadata].
         *
         * @param permissions The proto file metadata to convert.
         *
         * @return The converted file metadata.
         */
        fun fromProto(permissions: FileMetaData): FileMetadata = FileMetadata(
            uid = permissions.uid,
            gid = permissions.gid,
            userPermissions = FilePermissions.fromProto(permissions.userPermissions),
            groupPermissions = FilePermissions.fromProto(permissions.groupPermissions),
            othersPermissions = FilePermissions.fromProto(permissions.othersPermissions),
        )

        /**
         * Creates an empty metadata object.
         *
         * @return The empty metadata object.
         */
        fun createEmptyMetadata() = FileMetadata(
            uid = 0,
            gid = 0,
            userPermissions = FilePermissions.createEmptyPermissions(),
            groupPermissions = FilePermissions.createEmptyPermissions(),
            othersPermissions = FilePermissions.createEmptyPermissions(),
        )
    }
}
