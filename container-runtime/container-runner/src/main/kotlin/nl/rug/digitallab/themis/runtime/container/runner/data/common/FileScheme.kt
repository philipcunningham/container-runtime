package nl.rug.digitallab.themis.runtime.container.runner.data.common

import nl.rug.protospec.judgement.v1.FileScheme as ProtoScheme

/**
 * An enum class representing the scheme of a file.
 * Available schemes are:
 * - STREAM: The file is a stream (stdin, stdout, stderr).
 * - FILE: The file is a file on the filesystem.
 */
enum class FileScheme {
    STREAM,
    FILE;

    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): ProtoScheme {
        return when (this) {
            STREAM -> ProtoScheme.STREAM
            FILE -> ProtoScheme.FILE
        }
    }

    companion object {
        /**
         * Convert a ProtoScheme to a [FileScheme].
         *
         * @param scheme The proto scheme to convert.
         *
         * @return The converted scheme.
         */
        fun fromProto(scheme: ProtoScheme): FileScheme {
            return when (scheme) {
                ProtoScheme.STREAM -> STREAM
                ProtoScheme.FILE -> FILE
                else -> throw IllegalArgumentException("Unknown scheme: $scheme")
            }
        }
    }
}
