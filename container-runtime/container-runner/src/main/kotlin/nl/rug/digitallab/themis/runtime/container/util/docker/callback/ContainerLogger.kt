package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import com.github.dockerjava.api.async.ResultCallbackTemplate
import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.api.model.StreamType
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.themis.runtime.container.runner.data.result.OutputFile
import org.jboss.logging.Logger

/**
 * A callback to process the output of the log command.
 *
 * @property containerId The ID of the container to log.
 * @property maxOutput The maximum number of bytes to allow on stdout + stderr.
 *
 * @property onOutputExceeded A callback to run when the output limit is exceeded.
 */
class ContainerLogger(
    private val containerId: String,
    private val maxOutput: ByteSize,
    val onOutputExceeded: () -> Unit
) : ResultCallbackTemplate<ContainerLogger, Frame>() {
    private val logger: Logger = Logger.getLogger(ContainerLogger::class.java)

    private var stdout = byteArrayOf()
    private var stderr = byteArrayOf()

    /**
     * Called whenever a message is received.
     *
     * @param receivedMsg The message received.
     */
    override fun onNext(receivedMsg: Frame?) {
        // Check if the message is valid
        if (receivedMsg == null || receivedMsg.payload == null || receivedMsg.payload.isEmpty()) {
            logger.warn("Container $containerId sent an invalid message to the logger: \"${receivedMsg?.toString()}\"")
            return
        }

        // Check if the output limit is exceeded. Do not add the message to the output if it is to protect against enormous single payloads
        if ((stdout.size + stderr.size + receivedMsg.payload.size) > maxOutput.B) {
            onOutputExceeded()
        }

        when(receivedMsg.streamType) {
            StreamType.STDOUT -> {
                stdout += receivedMsg.payload
            }
            StreamType.STDERR -> {
                stderr += receivedMsg.payload
            }

            else -> {
                logger.error("Container $containerId sent message on unknown channel: \"$receivedMsg\"")
                return
            }
        }
    }

    /**
     * Called whenever an error occurs.
     *
     * @param throwable The error that occurred.
     */
    override fun onError(throwable: Throwable?) {
        logger.error("Container $containerId sent an error message to the logger: \"${throwable?.message}\"")
    }

    /**
     * Get the stdout of the container.
     *
     * @return The stdout of the container.
     */
    fun getStdout() = OutputFile.fromOutStream("stdout", stdout)

    /**
     * Get the stderr of the container.
     *
     * @return The stderr of the container.
     */
    fun getStderr() = OutputFile.fromOutStream("stderr", stderr)

    /**
     * Get the total output size of the container in KB.
     *
     * @return The total output size of the container.
     */
    fun outputSize(): ByteSize = (stdout.size.toLong() + stderr.size.toLong()).B
}
