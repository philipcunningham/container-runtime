package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import com.github.dockerjava.api.async.ResultCallbackTemplate
import org.jboss.logging.Logger

/**
 * A callback to process the output of generic docker commands.
 *
 * @property T The type of the response.
 */
open class GenericCallback<T> : ResultCallbackTemplate<GenericCallback<T>, T>() {
    private val logger = Logger.getLogger("GenericCallback")

    var error: Throwable? = null

    /**
     * Called whenever a message is received.
     */
    override fun onNext(response: T?) {
        logger.info("Callback received message: \"${response?.toString()}\"")
    }

    /**
     * Called whenever an error occurs.
     */
    override fun onError(throwable: Throwable?) {
        logger.error("Callback produced error: \"${throwable?.message}\"")

        error = throwable

        onComplete()
    }
}
