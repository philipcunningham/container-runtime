package nl.rug.digitallab.themis.runtime.container.runner.data.result

import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.executionResult
import nl.rug.protospec.judgement.v1.runActionResponse

/**
 * The result of an execution.
 *
 * @param status The status of the execution.
 * @param exitCode The exit code of the execution.
 * @param files The files extracted from the container.
 * @param resourceUsage The resources used by the execution (wall time, memory, etc.).
 */
data class ContainerResult(
    val status: ContainerStatus,
    val exitCode: Int,
    val files: List<OutputFile>,
    val resourceUsage: ResourceUsage,
) {
    /**
     * Convert this object to a [RunActionResponse] proto.
     */
    fun toProto(): RunActionResponse =
        runActionResponse {
            result = executionResult {
                status = this@ContainerResult.status.toProto()
                exitCode = this@ContainerResult.exitCode
                outputFiles += files.map { it.toProto() }
            }
            usage = resourceUsage.toProto()
        }
}
