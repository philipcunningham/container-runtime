package nl.rug.digitallab.themis.runtime.container.runner.data.common

import com.google.protobuf.ByteString
import nl.rug.protospec.judgement.v1.file
import nl.rug.protospec.judgement.v1.File as ProtoFile

/**
 * An implementation of a file that is to be inserted or retrieved from the container.
 *
 * @param scheme The scheme of the file (file or stream).
 * @param path The path of the file.
 * @param content The content of the file.
 * @param metadata The metadata of the file (permissions and ids).
 */
data class ContainerFile(
    val scheme: FileScheme,
    val path: String,
    val content: ByteArray,
    val metadata: FileMetadata,
) {
    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): ProtoFile = file {
        scheme = this@ContainerFile.scheme.toProto()
        path = this@ContainerFile.path
        contents = ByteString.copyFrom(content)
        meta = metadata.toProto()
    }

    companion object {
        /**
         * Convert a [ProtoFile] to a [ContainerFile].
         *
         * @param file The gRPC proto file to convert.
         *
         * @return The converted file.
         */
        fun fromProto(file: ProtoFile) = ContainerFile(
            scheme = FileScheme.fromProto(file.scheme),
            path = file.path,
            content = file.contents.toByteArray(),
            metadata = FileMetadata.fromProto(file.meta),
        )

        /**
         * Creates an empty file.
         *
         * @param name The name of the file.
         *
         * @return The empty file.
         */
        fun createEmptyFile(name: String) = ContainerFile(
            scheme = FileScheme.FILE,
            path = name,
            content = ByteArray(0),
            metadata = FileMetadata.createEmptyMetadata(),
        )
    }
}
