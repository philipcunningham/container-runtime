package nl.rug.digitallab.themis.runtime.container.services

import io.quarkus.grpc.GrpcService
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import kotlinx.coroutines.*
import nl.rug.digitallab.common.quarkus.opentelemetry.contextualUni
import nl.rug.digitallab.themis.runtime.container.runner.ContainerRunner
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ContainerConfiguration
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ResourceLimitsConfig
import nl.rug.protospec.judgement.v1.ActionRunner
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.jboss.logging.Logger

/**
 * The gRPC service for handling the RunAction request from the judgement manager.
 */
@GrpcService
class ActionService : ActionRunner {
    @Inject
    private lateinit var containerRunner: ContainerRunner

    @Inject
    private lateinit var limitsConfig: ResourceLimitsConfig

    @Inject
    private lateinit var logger: Logger

    /**
     * Actually receives the gRPC call. Will start the container and return the result of execution.
     *
     * @param request The request from the judgement manager.
     *
     * @throws IllegalArgumentException If the request is null.
     *
     * @return The response to the judgement manager.
     */
    @OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
    override fun runAction(request: RunActionRequest?): Uni<RunActionResponse> = contextualUni {
        logger.info("Received request:" + System.lineSeparator() + request)

        requireNotNull(request) { "gRPC Request is null" }

        containerRunner.runContainer(
            containerConfiguration = ContainerConfiguration.fromProto(request, limitsConfig)
        ).toProto()
    }
}
