package nl.rug.digitallab.themis.runtime.container.runner.data.result

import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileMetadata
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FilePermissions
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileScheme
import nl.rug.protospec.judgement.v1.OutputFile as ProtoOutputFile

/**
 * A single file extracted from the container.
 */
data class OutputFile (
    val file: ContainerFile,
    val isPresent: Boolean,
    val sizeExceeded: Boolean,
) {
    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): ProtoOutputFile = ProtoOutputFile.newBuilder()
        .setFile(file.toProto())
        .setIsPresent(isPresent)
        .setExceededSize(sizeExceeded)
        .build()

    companion object {
        /**
         * Convert one of the container's output streams to an [OutputFile].
         *
         * @param name The name of the stream.
         * @param content The binary content of the stream.
         *
         * @return The stream, represented as an [OutputFile].
         */
        fun fromOutStream(
            name: String,
            content: ByteArray,
        ) = OutputFile(
            file = ContainerFile(
                scheme = FileScheme.STREAM,
                path = name,
                content = content,
                metadata = FileMetadata(
                    uid = 0,
                    gid = 0,
                    userPermissions = FilePermissions(read = true, write = false, execute = false),
                    groupPermissions = FilePermissions(read = true, write = false, execute = false),
                    othersPermissions = FilePermissions(read = true, write = false, execute = false),
                )
            ),
            isPresent = true,
            sizeExceeded = false,
        )

        /**
         * Creates an [OutputFile] that represents a file that was not present in the container.
         *
         * @param name The name of the file.
         *
         * @return The missing file, represented as an [OutputFile].
         */
        fun nonPresent(name: String) = OutputFile(
            file = ContainerFile.createEmptyFile(name),
            isPresent = false,
            sizeExceeded = false,
        )

        /**
         * Creates an [OutputFile] that represents a file that was too large to be extracted from the container.
         *
         * @param name The name of the file.
         *
         * @return The too large file, represented as an [OutputFile].
         */
        fun tooLarge(name: String) = OutputFile(
                file = ContainerFile.createEmptyFile(name),
                isPresent = true,
                sizeExceeded = true,
            )
    }
}
