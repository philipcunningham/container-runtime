package nl.rug.digitallab.themis.runtime.container.runner.data.common

import nl.rug.protospec.judgement.v1.filePermissions

/**
 * The permissions of a file.
 *
 * @param read Whether the file can be read.
 * @param write Whether the file can be written to.
 * @param execute Whether the file can be executed.
 */
data class FilePermissions(
    val read: Boolean,
    val write: Boolean,
    val execute: Boolean,
) {
    val bitmask: Int
        get() =
            (if (read) 1 shl 2 else 0) or
            (if (write) 1 shl 1 else 0) or
            (if (execute) 1 shl 0 else 0)

    /**
     * [FilePermissions] constructor that sets its permission using a POSIX permission integer.
     *
     * @param bitmask The POSIX permissions of the file encoded as an integer.
     */
    constructor(
        bitmask: Int,
    ) : this(
        read = (bitmask shr 2) % 2 == 1,
        write = (bitmask shr 1) % 2 == 1,
        execute = (bitmask shr 0) % 2 == 1,
    )

    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): nl.rug.protospec.judgement.v1.FilePermissions = filePermissions {
        read = this@FilePermissions.read
        write = this@FilePermissions.write
        execute = this@FilePermissions.execute
    }

    companion object {
        /**
         * Convert a ProtoFilePermissions to a [FilePermissions].
         *
         * @param permissions The gRPC proto file permissions to convert.
         *
         * @return The converted file permissions.
         */
        fun fromProto(permissions: nl.rug.protospec.judgement.v1.FilePermissions): FilePermissions = FilePermissions(
            read = permissions.read,
            write = permissions.write,
            execute = permissions.execute,
        )

        /**
         * Creates an empty permission set.
         *
         * @return The empty permission set.
         */
        fun createEmptyPermissions() = FilePermissions(read = false, write = false, execute = false)
    }
}
