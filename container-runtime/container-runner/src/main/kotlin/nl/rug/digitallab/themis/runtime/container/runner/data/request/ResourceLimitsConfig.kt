package nl.rug.digitallab.themis.runtime.container.runner.data.request

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.ByteSizeLongConverter

/**
 * The configuration for the resource limits.
 * Includes the minimum values for the limits for the container to function properly.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.limits")
interface ResourceLimitsConfig {
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("min-memory")
    fun minMemory(): ByteSize
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("min-disk")
    fun minDisk(): ByteSize
    fun minWallTime(): Int
    fun minCpus(): Float
    fun minContainerProcesses(): Int
    fun minUserProcesses(): Int
}
