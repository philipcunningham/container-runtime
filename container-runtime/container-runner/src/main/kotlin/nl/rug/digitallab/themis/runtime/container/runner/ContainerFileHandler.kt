package nl.rug.digitallab.themis.runtime.container.runner

import com.github.dockerjava.api.exception.NotFoundException
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.runtime.container.exceptions.FileTooLargeException
import nl.rug.digitallab.themis.runtime.container.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.request.OutputFileRequest
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerStatus
import nl.rug.digitallab.themis.runtime.container.runner.data.result.OutputFile
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.TarHelper
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream

/**
 * This class is responsible for inserting files into and extracting files from the container.
 */
@ApplicationScoped
class ContainerFileHandler {
    @Inject
    private lateinit var dockerManager: DockerManager

    /**
     * Insert a list of files into a container.
     *
     * @param files The files to insert into the container.
     */
    context(ContainerContext)
    suspend fun insertFiles(files: List<ContainerFile>) = withCoroutineSpan("INSERT files", Dispatchers.IO) {
        val inputStream = PipedInputStream()

        inputStream.use { stream ->
            PipedOutputStream(stream).use {outputStream ->
                launch { TarHelper.insertFilesIntoStreamAsTar(files, outputStream) }
                val copyJob = launch { dockerManager.copyArchiveIntoContainer(stream, containerId) }

                // Wait for the copy into the container to finish
                // so the input stream isn't closed before the copy is done
                copyJob.join()
            }
        }
    }

    /**
     * Retrieves a single [ContainerFile] from a container.
     *
     * @param requestedFile The request describing the file to retrieve from the container.
     *
     * @return The file retrieved from the container.
     */
    context(ContainerContext)
    fun retrieveFile(requestedFile: OutputFileRequest): ContainerFile {
        // Retrieve the file from the container
        val tarStream = dockerManager.copyArchiveFromContainer(requestedFile.path, containerId)

        tarStream.use { stream ->
            // Convert the stream to a tar archive stream so that we can access the Tar Header
            TarArchiveInputStream(stream).use {convertedStream ->
                // Extract the file from the tar archive stream. Note that the tar stream will only contain one file.
                return TarHelper.extractFileFromTarStream(convertedStream, requestedFile.path, requestedFile.maxSize)
            }
        }
    }

    /**
     * Retrieve a list of files from a container.
     *
     * @param requestedFiles The files to retrieve from the container.
     *
     * @return A class representing the function result, containing:
     * 1. The list of files retrieved from the container.
     * 2. A boolean indicating whether one of the files was too large to retrieve.
     */
    context(ContainerContext)
    @WithSpan("RETRIEVE files")
    fun retrieveFiles(requestedFiles: List<OutputFileRequest>): List<OutputFile> {
        val outputFiles = mutableListOf<OutputFile>()

        for (requestedFile in requestedFiles) {
            try {
                outputFiles.add(OutputFile(
                    file = retrieveFile(requestedFile),
                    isPresent = true,
                    sizeExceeded = false,
                ))
            } catch (e: NotFoundException) {
                // If the file is not present in the container, add a file marked as non-present
                outputFiles.add(OutputFile.nonPresent(requestedFile.path))
            } catch (e: FileTooLargeException) {
                // If the file is too large, add a file marked as too large
                outputFiles.add(OutputFile.tooLarge(requestedFile.path))
                reportStatus(ContainerStatus.EXCEEDED_OUTPUT_FILE)
            }
        }

        return outputFiles
    }
}
