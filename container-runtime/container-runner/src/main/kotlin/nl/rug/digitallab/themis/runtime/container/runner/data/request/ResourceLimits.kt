package nl.rug.digitallab.themis.runtime.container.runner.data.request

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.disk
import nl.rug.protospec.judgement.v1.ioStreams
import nl.rug.protospec.judgement.v1.memory

/**
 * The resource limits for a container.
 * These limits are used to limit the resources a container can use.
 * Used both to protect the system from malicious containers, and to evaluate the efficiency of student code.
 * 
 * @param wallTime The maximum amount of time the container can run in real time. In milliseconds.
 * @param cpuCores The maximum amount of CPU cores the container can use. Fractional values are allowed.
 * @param containerProcesses The maximum amount of processes the container can use.
 * @param userProcesses The maximum amount of processes controlled by the user the container can use. (So excludes gVisor processes, etc.)
 * @param memoryLimit The maximum amount of memory the container can use.
 * @param diskLimit The maximum amount of disk space the container can use.
 * @param maxOutput The maximum amount of stdout + stderr the container can produce.
 */
class ResourceLimits private constructor (
    val wallTime: Int,   // Milliseconds

    val cpuCores: Float,

    val containerProcesses: Int,
    val userProcesses: Int,

    val memoryLimit: ByteSize,
    val diskLimit: ByteSize,
    val maxOutput: ByteSize,
) {
    val internalProcessCount: Int = 1

    companion object {
        /**
         * Create a new [ResourceLimits] object.
         *
         * @param wallTime The maximum amount of time the container can run in real time.
         * @param cpuCores The maximum amount of CPU cores the container can use.
         * @param containerProcesses The maximum amount of processes the container can use.
         * @param userProcesses The maximum amount of processes controlled by the user the container can use. (So excludes gVisor processes, etc.)
         * @param memoryLimit The maximum amount of memory the container can use.
         * @param diskLimit The maximum amount of disk space the container can use.
         * @param maxOutput The maximum amount of stdout + stderr the container can produce.
         * @param limitsConfig The minimum values for the limits.
         */
        fun build(
            wallTime: Int,        // Milliseconds
            cpuCores: Float,
            containerProcesses: Int,
            userProcesses: Int,
            memoryLimit: ByteSize,
            diskLimit: ByteSize,
            maxOutput: ByteSize,
            limitsConfig: ResourceLimitsConfig
        ): ResourceLimits {
            require(memoryLimit >= limitsConfig.minMemory())
            require(diskLimit >= limitsConfig.minDisk())
            require(wallTime >= limitsConfig.minWallTime())
            require(cpuCores >= limitsConfig.minCpus())
            require(containerProcesses >= limitsConfig.minContainerProcesses())
            require(userProcesses >= limitsConfig.minUserProcesses())

            return ResourceLimits(
                wallTime,
                cpuCores,
                containerProcesses,
                userProcesses,
                memoryLimit,
                diskLimit,
                maxOutput,
            )
        }

        /**
         * Create a new [ResourceLimits] object from a [nl.rug.protospec.judgement.v1.ResourceLimits] object.
         *
         * @param proto The [nl.rug.protospec.judgement.v1.ResourceLimits] object to convert.
         * @param config The minimum values for the limits.
         *
         * @return The converted [ResourceLimits] object.
         */
        fun fromProto(proto: nl.rug.protospec.judgement.v1.ResourceLimits, config: ResourceLimitsConfig): ResourceLimits =
            build(
                wallTime = proto.wallTimeMs,
                cpuCores = proto.cpuCores,
                containerProcesses = proto.containerProcesses,
                userProcesses =  proto.userProcesses,
                memoryLimit = proto.memory,
                diskLimit = proto.disk,
                maxOutput = proto.ioStreams,
                limitsConfig = config
            )
    }
}

