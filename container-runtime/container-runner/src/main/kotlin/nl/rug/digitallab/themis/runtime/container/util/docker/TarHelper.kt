package nl.rug.digitallab.themis.runtime.container.util.docker

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.themis.runtime.container.exceptions.FileTooLargeException
import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileMetadata
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileScheme
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import java.io.OutputStream

/**
 * This class helps with creating tar archives for copying files into the container.
 */
object TarHelper {
    /**
     * Creates a tar archive input stream so the container file can be copied into the container.
     * This function closes the output stream after it is done.
     *
     * @param containerFiles The list of files to insert into the tar archive.
     * This allows a container unpacking the tar archive to insert all the files into the container.
     * @param outputStream The output stream to write the tar archive to.
     * @param dispatcher The dispatcher to run the file insertion on.
     *
     * @return The tar archive input stream.
     */
    suspend fun insertFilesIntoStreamAsTar(
        containerFiles: List<ContainerFile>,
        outputStream: OutputStream,
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
    ) = withContext(dispatcher) {
        // Create the stream to write the tar file to, will be passed into the provided output stream
        TarArchiveOutputStream(outputStream).use { tarStream ->
            containerFiles.forEach { file -> insertFile(file, tarStream) }
        }
    }

    /**
     * Extracts a [ContainerFile] from a [TarArchiveInputStream].
     *
     * @param stream The tar archive input stream to extract the file from.
     * @param path The path of the file to extract. Only used for creating the name of the container file.
     * @param maxSize The maximum size of the file to extract.
     *
     * @return The extracted container file.
     */
    fun extractFileFromTarStream(
        stream: TarArchiveInputStream,
        path: String,
        maxSize: ByteSize,
    ): ContainerFile {
        // Retrieve the tar header containing the metadata of the file
        val tarEntry = stream.nextEntry ?: throw IllegalStateException("Tar header of retrieved container file is not valid")

        // Verify that the file is not too large
        if(tarEntry.size > maxSize.B) throw FileTooLargeException()

        // Read the file content from the stream
        val content = stream.readNBytes(tarEntry.size.toInt())

        return ContainerFile(
            scheme = FileScheme.FILE,
            path = path,
            content = content,
            metadata = FileMetadata(
                uid = tarEntry.longUserId.toInt(),
                gid = tarEntry.longGroupId.toInt(),
                tarEntry.mode,
            ),
        )
    }

    /**
     * Creates a tar archive header to match the metadata of the container file.
     *
     * @param containerFile The container file to create the header for.
     *
     * @return The tar archive header entry.
     */
    private fun createTarEntry(containerFile: ContainerFile) =
        TarArchiveEntry(containerFile.path).apply {
            size = containerFile.content.size.toLong()
            mode = containerFile.metadata.posixPermissions
            groupId = containerFile.metadata.gid
            userId = containerFile.metadata.uid
        }

    /**
     * Inserts a single file into the tar archive.
     *
     * @param containerFile The file to insert into the tar archive.
     * @param tarStream The tar archive output stream to insert the file into.
     */
    private fun insertFile(containerFile: ContainerFile, tarStream: TarArchiveOutputStream) {
        val header = createTarEntry(containerFile)
        tarStream.putArchiveEntry(header)
        tarStream.write(containerFile.content)
        tarStream.closeArchiveEntry()
    }
}
