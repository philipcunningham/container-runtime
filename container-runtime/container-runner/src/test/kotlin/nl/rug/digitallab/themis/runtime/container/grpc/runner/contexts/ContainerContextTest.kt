package nl.rug.digitallab.themis.runtime.container.runner.contexts

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerStatus
import org.instancio.Instancio
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

/**
 * Tests for [ContainerContext].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class ContainerContextTest {
    val context = ContainerContext("testContainerId", Logger.getLogger("testLogger"))

    @Test
    fun `The context throws an error if the status is accessed before it is set`() {
        assertThrows(UninitializedPropertyAccessException::class.java) {
            context.containerExecStatus
        }
    }

    @ParameterizedTest(name = "The context can take a valid status and store it: {0}")
    @EnumSource(ContainerStatus::class)
    fun `The context can take a valid status and store it`(status: ContainerStatus) {
        context.reportStatus(status)
        assertEquals(status, context.containerExecStatus)
    }

    @Test
    fun `The context properly enforces the priority of the statuses`() {
        // Report 5 random statuses
        Instancio.ofList(ContainerStatus::class.java)
            .size(5)
            .create()
            .forEach(context::reportStatus)

        // Highest priority status reported
        context.reportStatus(ContainerStatus.EXCEEDED_WALL_TIME)

        assertEquals(ContainerStatus.EXCEEDED_WALL_TIME, context.containerExecStatus)
    }

    @Test
    fun `The container id is properly stored`() {
        assertEquals("testContainerId", context.containerId)
    }

    @Test
    fun `The exit code is properly stored`() {
        context.reportExitCode(25)
        assertEquals(25, context.exitCode)
    }
}
