package nl.rug.digitallab.themis.runtime.container.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.util.protoField
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import nl.rug.protospec.judgement.v1.File as ProtoFile
import nl.rug.protospec.judgement.v1.FileScheme as ProtoScheme

/**
 * Tests for [FilePermissions].
 */
@QuarkusTest
class ContainerFileTest {
    @Test
    fun `A proto file is properly parsed to an internal file representation`() {
        val protoFile = Instancio.of(ProtoFile::class.java)
            .generate(ProtoFile::getScheme.protoField()) { gen ->
                gen.oneOf(ProtoScheme.FILE.number, ProtoScheme.STREAM.number)
            }
            .create()
        val containerFile = ContainerFile.fromProto(protoFile)

        assertEquals(protoFile.scheme, containerFile.scheme.toProto())
        assertEquals(protoFile.path, containerFile.path)
        assertArrayEquals(protoFile.contents.toByteArray(), containerFile.content)
        assertEquals(FileMetadata.fromProto(protoFile.meta), containerFile.metadata)
    }
}
