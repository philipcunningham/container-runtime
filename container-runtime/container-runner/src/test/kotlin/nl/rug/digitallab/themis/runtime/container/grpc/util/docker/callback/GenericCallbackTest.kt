package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import com.github.dockerjava.api.model.WaitResponse
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * Tests for [GenericCallback].
 */
@QuarkusTest
class GenericCallbackTest {
    @Test
    fun `Generic callback does not flag an error when none have occurred`() {
        val callback = GenericCallback<WaitResponse>()
        callback.onNext(WaitResponse())
        assertNull(callback.error)
    }

    @Test
    fun `Generic callback flags errors when they have occurred`() {
        val callback = GenericCallback<WaitResponse>()
        callback.onNext(WaitResponse())
        callback.onError(Throwable())
        assertNotNull(callback.error)
    }

    @Test
    fun `Generic callback does not flag errors when nothing has happened yet`() {
        val callback = GenericCallback<WaitResponse>()
        assertNull(callback.error)
    }
}
