package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.api.model.StreamType
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.util.asTarget
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.TestInstance


/**
 * Tests for [ContainerLogger].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class ContainerLoggerTest {
    data class LoggerTestArgument(val streamType: StreamType, val bytesPassed: Int)

    private final val generator = Instancio.ofList(LoggerTestArgument::class.java)
        .size(50)
        .withSeed(761554)

    val frameData: ByteArray
        get() = Instancio.ofList(Byte::class.java)
            .size(100)
            .withSeed(761554)
            .create()
            .toByteArray()

    val logger = ContainerLogger(
        containerId = "",
        maxOutput = 100.KB,
        onOutputExceeded = {}
    )

    @Test
    fun `The stdout stream gets properly logged by the callback`() {
        logger.onNext(Frame(StreamType.STDOUT, frameData))

        val retrievedStdout = logger.getStdout()

        assertArrayEquals(frameData, retrievedStdout.file.content)
    }

    @Test
    fun `The stderr stream gets properly logged by the callback`() {
        logger.onNext(Frame(StreamType.STDERR, frameData))

        val retrievedStderr = logger.getStderr()

        assertArrayEquals(frameData, retrievedStderr.file.content)
    }

    @TestFactory
    fun `Output exceeded is false when the output limit hasn't been reached`(): List<DynamicTest> {
        return generator
            .generate(LoggerTestArgument::streamType.asTarget()) { gen ->
                gen.oneOf(StreamType.STDOUT, StreamType.STDERR)
            }
            .generate(LoggerTestArgument::bytesPassed.asTarget()) { gen ->
                // 0 B to 20 KB, will be inserted 5x into the array, so total should not exceed 100KB
                gen.ints().range(0, 20_000)
            }
            .create()
            .chunked(5)
            .map {
                DynamicTest.dynamicTest("Output exceeded is false when the output limit hasn't been reached: $it") {
                    var outputExceeded = false

                    val logger = ContainerLogger(
                        containerId = "",
                        maxOutput = 100.KB,
                        onOutputExceeded = {
                            outputExceeded = true
                        })

                    for (arg in it) {
                        logger.onNext(Frame(arg.streamType, ByteArray(size = arg.bytesPassed)))
                    }

                    assertFalse(outputExceeded)
                }
            }.toList()
    }


    @TestFactory
    fun `Output exceeded is true when the output limit has been reached`(): List<DynamicTest> {
        return generator
            .generate(LoggerTestArgument::streamType.asTarget()) { gen ->
                gen.oneOf(StreamType.STDOUT, StreamType.STDERR)
            }
            .generate(LoggerTestArgument::bytesPassed.asTarget()) { gen ->
                // 20 KB to 10 MB, will be inserted 5x into the array, so total should exceed 100KB
                gen.ints().range(20_001, 10_000_000)
            }
            .create()
            .chunked(5)
            .map {
            DynamicTest.dynamicTest("Output exceeded is false when the output limit hasn't been reached: $it") {
                var outputExceeded = false

                val logger = ContainerLogger(
                    containerId = "",
                    maxOutput = 100.KB,
                    onOutputExceeded = {
                        outputExceeded = true
                    })

                for (arg in it) {
                    logger.onNext(Frame(arg.streamType, ByteArray(size = arg.bytesPassed)))
                }

                assertTrue(outputExceeded)
            }
        }.toList()
    }
}
