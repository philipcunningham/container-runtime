package nl.rug.digitallab.themis.runtime.container.runner.data.request

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.digitallab.common.kotlin.quantities.MB
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

/**
 * Tests for [ResourceLimits].
 */
@QuarkusTest
class ResourceLimitsTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @ParameterizedTest(name = "Limits are properly built for a walltime of {0}")
    @ValueSource(ints = [100, 1000, 5000, 10000])
    fun `Limits are properly built for various walltime values`(wallTime: Int) {
        assertDoesNotThrow {
            ResourceLimits.build(
                wallTime = wallTime,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a cpu cores of {0}")
    @ValueSource(floats = [0.1f, 0.5f, 1.0f, 2.0f])
    fun `Limits are properly built for various cpu cores values`(cores: Float) {
        assertDoesNotThrow {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = cores,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a container processes of {0}")
    @ValueSource(ints = [5, 10, 20])
    fun `Limits are properly built for various container processes values`(processes: Int) {
        assertDoesNotThrow {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = processes,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a user processes of {0}")
    @ValueSource(ints = [5, 10, 20])
    fun `Limits are properly built for various user processes values`(processes: Int) {
        assertDoesNotThrow {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = processes,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a memory of {0}")
    @ValueSource(longs = [10000, 50000, 100000, 200000])
    fun `Limits are properly built for various memory values`(memory: Long) {
        assertDoesNotThrow {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = memory.KB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a disk of {0}")
    @ValueSource(ints = [0, 10, 50, 99])
    fun `Limits are not built for various bad walltime values`(wallTime: Int) {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = wallTime,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for various bad cpu core values of {0}")
    @ValueSource(floats = [0.0f, 0.01f, 0.09f, 0.099f])
    fun `Limits are not built for various bad cpu core values`(cores: Float) {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = cores,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @Test
    fun `Limits are not built for a bad container processes value`() {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 0,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig,
            )
        }
    }

    @Test
    fun `Limits are not built for a bad user processes value`() {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 0,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for various bad memory values of {0}")
    @ValueSource(longs = [0, 10, 100, 1000, 9999])
    fun `Limits are not built for various bad memory values`(memory: Long) {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = memory.KB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for various bad disk values of {0}")
    @ValueSource(longs = [0, 10, 100, 1000, 9999])
    fun `Limits are not built for various bad disk values`(disk: Long) {
        assertThrows<IllegalArgumentException> {
            ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = disk.KB,
                maxOutput = 5.MB,
                limitsConfig
            )
        }
    }
}
