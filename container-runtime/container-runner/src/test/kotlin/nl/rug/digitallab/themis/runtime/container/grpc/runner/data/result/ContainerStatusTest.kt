package nl.rug.digitallab.themis.runtime.container.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

/**
 * Tests for [ContainerStatus].
 */
@QuarkusTest
class ContainerStatusTest {
    @ParameterizedTest(name = "The status {0} is properly parsed to a proto status")
    @EnumSource(ContainerStatus::class)
    fun `An internal status is properly parsed to a proto status`(status: ContainerStatus) {
        val protoStatus = status.toProto()
        assertEquals(status.name, protoStatus.name)
    }

    @Test
    fun `A logical priority list is set up for the statuses`() {
        assertTrue(ContainerStatus.SUCCEEDED.toPriority() < ContainerStatus.CRASHED.toPriority())
        assertTrue(ContainerStatus.PRODUCED_ZOMBIES.toPriority() < ContainerStatus.PRODUCED_ZOMBIES_AND_ORPHANS.toPriority())
        assertTrue(ContainerStatus.PRODUCED_ZOMBIES_AND_ORPHANS.toPriority() < ContainerStatus.CRASHED.toPriority())
        assertTrue(ContainerStatus.CRASHED.toPriority() < ContainerStatus.EXCEEDED_PROCESSES.toPriority())
        assertTrue(ContainerStatus.EXCEEDED_PROCESSES.toPriority() < ContainerStatus.EXCEEDED_MEMORY.toPriority())
        assertTrue(ContainerStatus.EXCEEDED_MEMORY.toPriority() < ContainerStatus.EXCEEDED_WALL_TIME.toPriority())
    }
}
