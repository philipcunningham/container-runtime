package nl.rug.digitallab.themis.runtime.container.util.docker

import io.quarkus.test.junit.QuarkusTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileScheme
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import java.io.InputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream

/**
 * Tests for [TarHelper].
 */
@QuarkusTest
class TarhelperTest {
    /**
     * Test the output of the [TarHelper.insertFilesIntoStreamAsTar]'s stream, testing if it matches the input.
     *
     * @param inputStream The input stream, containing the tar archive, to test.
     * @param insertableFiles The list of files that should be in the tar archive.
     */
    suspend fun testStreamOutput(inputStream: InputStream, insertableFiles: List<ContainerFile>) = withContext(Dispatchers.IO) {
        inputStream.use { inputStream ->
            val tarStream = TarArchiveInputStream(inputStream)

            var nextEntry = tarStream.nextEntry
            var idx = 0
            while(nextEntry != null) {
                val file = insertableFiles[idx]
                assertNotNull(file)
                assertEquals(file.path, nextEntry.name)
                assertEquals(file.content.size.toLong(), nextEntry.size)
                assertEquals(file.metadata.posixPermissions, nextEntry.mode)
                assertEquals(file.metadata.gid.toLong(), nextEntry.longGroupId)
                assertEquals(file.metadata.uid.toLong(), nextEntry.longUserId)

                ++idx
                nextEntry = tarStream.nextEntry
            }
        }
    }

    @Test
    fun `The TarHelper can insert files into a stream as a TAR`()
    = runTest(StandardTestDispatcher()) {
        val insertableFiles = Instancio
            .ofList(ContainerFile::class.java)
            .size(5)
            .create()

        val inputStream = PipedInputStream()
        PipedOutputStream(inputStream).use { outputStream ->
            val insertionJob = launch { TarHelper.insertFilesIntoStreamAsTar(insertableFiles, outputStream) }
            launch { testStreamOutput(inputStream, insertableFiles) }

            insertionJob.join()
        }
    }

    @Test
    fun `The TarHelper can extract a file from a tar stream and parse it`() {
        val tarStream = mock<TarArchiveInputStream>()
        whenever(tarStream.nextEntry).thenReturn(
            TarArchiveEntry("test.txt").apply {
                size = 12
            },
        )
        whenever(tarStream.readNBytes(eq(12))).thenReturn("Test message".toByteArray())

        val file = TarHelper.extractFileFromTarStream(tarStream, "test.txt", 1.MB)
        assertEquals(FileScheme.FILE, file.scheme)
        assertEquals("test.txt", file.path)
        assertArrayEquals("Test message".toByteArray(), file.content)
    }
}
