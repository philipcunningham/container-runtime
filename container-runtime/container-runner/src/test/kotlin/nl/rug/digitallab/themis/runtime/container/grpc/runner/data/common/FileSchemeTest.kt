package nl.rug.digitallab.themis.runtime.container.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

import nl.rug.protospec.judgement.v1.FileScheme as ProtoScheme

@QuarkusTest
class FileSchemeTest {
    @Test
    fun `An internal file schema is properly parsed to a proto file schema representation`() {
        val file = FileScheme.FILE
        val stream = FileScheme.STREAM

        val protoFile = file.toProto()
        val protoStream = stream.toProto()

        assertEquals(ProtoScheme.FILE, protoFile)
        assertEquals(ProtoScheme.STREAM, protoStream)
    }

    @Test
    fun `A proto file schema is properly parsed to an internal file schema representation`() {
        val protoFile = ProtoScheme.FILE
        val protoStream = ProtoScheme.STREAM

        val file = FileScheme.fromProto(protoFile)
        val stream = FileScheme.fromProto(protoStream)

        assertEquals(FileScheme.FILE, file)
        assertEquals(FileScheme.STREAM, stream)
    }
}
