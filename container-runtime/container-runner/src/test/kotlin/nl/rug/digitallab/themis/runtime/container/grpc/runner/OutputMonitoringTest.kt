package nl.rug.digitallab.themis.runtime.container.runner

import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ResourceLimits
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.callback.ContainerLogger
import org.jboss.logging.Logger
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.Mockito
import org.mockito.kotlin.*

/**
 * Tests for [ContainerRunner]'s output monitoring.
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OutputMonitoringTest {
    lateinit var mockedManager: DockerManager

    @Inject
    lateinit var runner: ContainerRunner

    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    /**
     * Sets up the mocked docker manager.
     */
    @BeforeAll
    fun setup() {
        mockedManager = mock()

        // Mock the docker kill command
        doNothing().whenever(mockedManager).killContainer(any())

        // Mock the docker log command
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<ContainerLogger>(0)
            logger.onOutputExceeded()
            logger
        }

        QuarkusMock.installMockForType(mockedManager, DockerManager::class.java)
    }

    @Test
    fun `Docker kill is called when the container exceeds its output limit`() {
        with(ContainerContext("", Logger.getLogger("testLogger"))) {
            runner.monitorOutput(resourceLimits = ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
            )
            )
        }

        Mockito.verify(mockedManager).killContainer(any())
    }
}
