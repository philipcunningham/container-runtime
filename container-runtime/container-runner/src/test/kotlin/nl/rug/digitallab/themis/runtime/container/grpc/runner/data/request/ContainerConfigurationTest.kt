package nl.rug.digitallab.themis.runtime.container.runner.data.request

import com.google.protobuf.ByteString
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.protospec.judgement.v1.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [ContainerConfiguration].
 */
@QuarkusTest
class ContainerConfigurationTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @Test
    fun `A proto container configuration is properly parsed to an internal file container configuration`() {
        val protoRequest = runActionRequest {
            environment = "ubuntu"
            command += "cat"
            command += "test.txt"
            limits = resourceLimits {
                wallTimeMs = 5000
                cpuCores = 1f
                containerProcesses = 10
                userProcesses = 5
                memoryBytes = 100.MB.B
                diskBytes = 100.MB.B
                ioStreamsBytes = 100.MB.B
            }
            features = features {
                isTrusted = Trust.TRUSTED
            }
            inputFiles += file {
                scheme = FileScheme.FILE
                path = "test.txt"
                contents = ByteString.copyFromUtf8("This is the content of a file")
            }
        }

        val containerConfig = ContainerConfiguration.fromProto(protoRequest, limitsConfig)

        assertEquals(protoRequest.commandList, containerConfig.command)
        assertEquals(protoRequest.environment, containerConfig.image)
        assertEquals(protoRequest.limits.cpuCores, containerConfig.resourceLimits.cpuCores)
        assertEquals(protoRequest.limits.diskBytes, containerConfig.resourceLimits.diskLimit.B)
        assertEquals(protoRequest.limits.ioStreamsBytes, containerConfig.resourceLimits.maxOutput.B)
        assertEquals(protoRequest.limits.memoryBytes, containerConfig.resourceLimits.memoryLimit.B)
        assertEquals(protoRequest.limits.containerProcesses, containerConfig.resourceLimits.containerProcesses)
        assertEquals(protoRequest.limits.wallTimeMs, containerConfig.resourceLimits.wallTime)
        assertEquals(protoRequest.features.isTrusted == Trust.TRUSTED, containerConfig.trusted)
        assertEquals(protoRequest.envVarsMap.size, containerConfig.envVars.size)
        assertEquals(protoRequest.inputFilesList.size, containerConfig.inputFiles.size)
    }
}
