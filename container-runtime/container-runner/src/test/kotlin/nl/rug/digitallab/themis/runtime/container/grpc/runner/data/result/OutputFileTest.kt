package nl.rug.digitallab.themis.runtime.container.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileScheme
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

/**
 * Tests for [OutputFile].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OutputFileTest {
    @Test
    fun `An internal OutputFile is properly parsed to a proto OutputFile`() {
        val file: OutputFile = Instancio.create(OutputFile::class.java)
        val protoFile = file.toProto()

        assertEquals(protoFile.file, file.file.toProto())
        assertEquals(protoFile.isPresent, file.isPresent)
        assertEquals(protoFile.exceededSize, file.sizeExceeded)
    }

    @Test
    fun `Correct implementation of non-present files`() {
        val file = OutputFile.nonPresent("test")

        assertFalse(file.isPresent)
        assertFalse(file.sizeExceeded)
        assertEquals(FileScheme.FILE, file.file.scheme)
        assertEquals("test", file.file.path)
    }

    @Test
    fun `Correct implementation of files that are too large`() {
        val file = OutputFile.tooLarge("test")

        assertTrue(file.isPresent)
        assertTrue(file.sizeExceeded)
        assertEquals(FileScheme.FILE, file.file.scheme)
        assertEquals("test", file.file.path)
    }

    @Test
    fun `A file can be created from an output stream`() {
        val file = OutputFile.fromOutStream("test", "test".toByteArray())

        assertTrue(file.isPresent)
        assertFalse(file.sizeExceeded)
        assertEquals(FileScheme.STREAM, file.file.scheme)
        assertEquals("test", file.file.path)
        assertEquals("test", String(file.file.content))
    }
}
