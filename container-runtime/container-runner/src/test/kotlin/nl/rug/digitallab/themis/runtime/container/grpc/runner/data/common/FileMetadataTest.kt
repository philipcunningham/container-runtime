package nl.rug.digitallab.themis.runtime.container.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.protospec.judgement.v1.FileMetaData
import nl.rug.util.compareFilePermissions
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class FileMetadataTest {
    @Test
    fun `A proto metadata is properly parsed to an internal metadata representation`() {
        val protoFileMeta = Instancio.create(FileMetaData::class.java)
        val containerFileMeta = FileMetadata.fromProto(protoFileMeta)

        Assertions.assertEquals(protoFileMeta.uid, containerFileMeta.uid)
        Assertions.assertEquals(protoFileMeta.gid, containerFileMeta.gid)
        compareFilePermissions(protoFileMeta.userPermissions, containerFileMeta.userPermissions)
        compareFilePermissions(protoFileMeta.groupPermissions, containerFileMeta.groupPermissions)
        compareFilePermissions(protoFileMeta.othersPermissions, containerFileMeta.othersPermissions)
    }

    @Test
    fun `The correct posix integer value is generated from the file permissions`() {
        val perms = FileMetadata(
            1,
            2,
            userPermissions = FilePermissions(read = true, write = true, execute = true),
            groupPermissions = FilePermissions(read = true, write = false, execute = true),
            othersPermissions = FilePermissions(read = true, write = false, execute = false)
        )
        val posix = perms.posixPermissions

        Assertions.assertEquals(492, posix)
    }
}
