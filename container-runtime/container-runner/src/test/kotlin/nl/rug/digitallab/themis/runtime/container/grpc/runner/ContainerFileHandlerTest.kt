package nl.rug.digitallab.themis.runtime.container.runner

import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.runtime.container.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.runtime.container.runner.data.common.ContainerFile
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileMetadata
import nl.rug.digitallab.themis.runtime.container.runner.data.common.FileScheme
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.any
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.PipedInputStream

/**
 * Tests for [ContainerFileHandler].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ContainerFileHandlerTest {
    lateinit var mockedManager: DockerManager

    @Inject
    lateinit var fileHandler: ContainerFileHandler

    /**
     * Set up the mocked docker manager.
     */
    @BeforeAll
    fun setup() {
        mockedManager = mock()

        runBlocking {
            // Receives a request to upload a tar archive, and checks whether it contains the "Test message" message passed in the unit test.
            // Used for the insertFilesTest
            whenever(mockedManager.copyArchiveIntoContainer(any(), any())).then {
                val stream = it.getArgument<PipedInputStream>(0)
                TarArchiveInputStream(stream).use { tarStream ->
                    val header = tarStream.nextEntry
                    val content = stream.readNBytes(header.size.toInt())
                    assertArrayEquals(content, "Test message".toByteArray())

                    stream.readBytes()
                }
            }
        }

        // Mock the docker kill command
        doNothing().`when`(mockedManager).killContainer("containerID")

        QuarkusMock.installMockForType(mockedManager, DockerManager::class.java)
    }

    @Test
    fun `A file can be inserted into a container`() {
        with(ContainerContext("", Logger.getLogger("testLogger"))) {
            runBlocking {
                // Will fail if the mocked docker manager does not receive a tar archive containing the "Test message" message
                fileHandler.insertFiles(
                    listOf(
                        ContainerFile(
                            FileScheme.FILE,
                            "test.txt",
                            "Test message".toByteArray(),
                            FileMetadata(
                                0,
                                0,
                                0,
                            )
                        )
                    )
                )
            }
        }
    }
}
