package nl.rug.digitallab.themis.runtime.container.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [ContainerResult].
 */
@QuarkusTest
class ContainerResultTest {
    @Test
    fun `An internal result is properly parsed to a proto result`() {
        val result = ContainerResult(
            ContainerStatus.SUCCEEDED,
            0,
            listOf(),
            ResourceUsage(),
        )
        val protoResult = result.toProto()

        assertEquals(result.exitCode, protoResult.result.exitCode)
        assertEquals(result.status.toProto(), protoResult.result.status)
        assertEquals(result.files.size, protoResult.result.outputFilesCount)
    }
}
