package nl.rug.digitallab.themis.runtime.container.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.util.compareFilePermissions
import org.instancio.Instancio
import org.junit.jupiter.api.Test

@QuarkusTest
class FilePermissionsTest {
    @Test
    fun `A proto permissions is properly parsed to an internal permissions representation`() {
        val protoPerms = Instancio.create(nl.rug.protospec.judgement.v1.FilePermissions::class.java)
        val containerPerms = FilePermissions.fromProto(protoPerms)

        compareFilePermissions(protoPerms, containerPerms)
    }
}
