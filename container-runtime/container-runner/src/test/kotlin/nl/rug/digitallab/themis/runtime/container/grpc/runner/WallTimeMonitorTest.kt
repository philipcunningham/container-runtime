package nl.rug.digitallab.themis.runtime.container.runner

import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.contexts.ContainerContext
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ResourceLimits
import nl.rug.digitallab.themis.runtime.container.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerStatus
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import org.jboss.logging.Logger
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*

/**
 * Tests for [ContainerRunner]'s wall time monitor.
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WallTimeMonitorTest {
    @Inject
    lateinit var runner: ContainerRunner

    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    /**
     * Set up the mocked docker manager.
     */
    @BeforeAll
    fun setup() {
        val mockedManager = Mockito.mock(DockerManager::class.java)

        // Mock the docker wait command as a container which takes 2s to finish
        whenever(mockedManager.waitContainer(any())).then {
            Thread.sleep(2000)
            0 // Exit code
        }

        // Mock the docker kill command
        doNothing().`when`(mockedManager).killContainer(any())

        QuarkusMock.installMockForType(mockedManager, DockerManager::class.java)
    }

    @Test
    fun `A container is flagged as exceeded wall time if it finishes too late`() = runBlocking {
        with(ContainerContext("", Logger.getLogger("testLogger"))) {
            runner.monitorWallTime(resourceLimits = ResourceLimits.build(
                wallTime = 1000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig
                )
            )

            assertEquals(ContainerStatus.EXCEEDED_WALL_TIME, containerExecStatus)
        }
    }

    @Test
    fun `A container is not flagged as exceeded wall time if it finishes on time`() = runBlocking {
        with(ContainerContext("", Logger.getLogger("testLogger"))) {
            runner.monitorWallTime(resourceLimits = ResourceLimits.build(
                wallTime = 3000,
                cpuCores = 1.0f,
                containerProcesses = 5,
                userProcesses = 5,
                memoryLimit = 50.MB,
                diskLimit = 50.MB,
                maxOutput = 5.MB,
                limitsConfig)
            )

            assertEquals(ContainerStatus.SUCCEEDED, containerExecStatus)
        }
    }
}
