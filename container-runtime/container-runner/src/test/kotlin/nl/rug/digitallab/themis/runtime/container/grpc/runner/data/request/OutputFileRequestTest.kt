package nl.rug.digitallab.themis.runtime.container.runner.data.request

import io.quarkus.test.junit.QuarkusTest
import org.instancio.Instancio
import org.junit.jupiter.api.Test

/**
 * Tests the [OutputFileRequest] class.
 */
@QuarkusTest
class OutputFileRequestTest {
    @Test
    fun `A proto OutputFileRequest is properly parsed to an internal OutputFileRequest`() {
        val proto = Instancio
            .of(nl.rug.protospec.judgement.v1.OutputFileRequest::class.java)
            .create()

        val request = OutputFileRequest.fromProto(proto)

        assert(request.path == proto.path)
        assert(request.maxSize.B == proto.maxSizeBytes)
    }
}
