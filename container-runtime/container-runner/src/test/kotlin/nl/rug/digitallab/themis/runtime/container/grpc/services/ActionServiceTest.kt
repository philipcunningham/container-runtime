package nl.rug.digitallab.themis.runtime.container.services

import io.quarkus.grpc.GrpcService
import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.ContainerRunner
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerResult
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ContainerStatus
import nl.rug.digitallab.themis.runtime.container.runner.data.result.ResourceUsage
import nl.rug.digitallab.themis.runtime.container.services.ActionService
import nl.rug.protospec.judgement.v1.resourceLimits
import nl.rug.protospec.judgement.v1.runActionRequest
import org.junit.jupiter.api.*
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.time.Duration

/**
 * Tests for [ActionService].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ActionServiceTest {
    @GrpcService
    lateinit var service: ActionService

    /**
     * Sets up the mocked container runner.
     * Basically makes the startContainer method do nothing.
     */
    @BeforeAll
    fun setup() {
        val mockedRunner: ContainerRunner = mock()

        runBlocking {
            whenever(mockedRunner.runContainer(any())).then {
                ContainerResult(ContainerStatus.SUCCEEDED, 0, listOf(), ResourceUsage())
            }
        }

        QuarkusMock.installMockForType(mockedRunner, ContainerRunner::class.java)
    }

    @Test
    fun `The ActionService correctly deals with an invalid gRPC request`() {
        assertThrows<IllegalArgumentException> {
            service.runAction(null).await().atMost(Duration.ofSeconds(1))
        }
    }

    @Test
    fun `The ActionService correctly deals with a valid gRPC request`() {
        assertDoesNotThrow {
            service.runAction(
                runActionRequest {
                    limits = resourceLimits {
                        wallTimeMs = 1000
                        cpuCores = 1.0f
                        containerProcesses = 5
                        userProcesses = 1
                        memoryBytes = 50.MB.B
                        diskBytes = 50.MB.B
                    }
                }
            ).await().atMost(Duration.ofSeconds(1))
        }
    }
}
