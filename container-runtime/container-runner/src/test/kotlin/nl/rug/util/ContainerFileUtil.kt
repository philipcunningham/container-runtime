package nl.rug.util

import nl.rug.digitallab.themis.runtime.container.runner.data.common.FilePermissions
import org.junit.jupiter.api.Assertions

/**
 * Compares the permissions of a proto file permissions and a container file permissions.
 *
 * @param protoPerms The proto file permissions.
 * @param containerPerms The container file permissions.
 */
fun compareFilePermissions(protoPerms: nl.rug.protospec.judgement.v1.FilePermissions, containerPerms: FilePermissions) {
    Assertions.assertEquals(protoPerms.execute, containerPerms.execute)
    Assertions.assertEquals(protoPerms.read, containerPerms.read)
    Assertions.assertEquals(protoPerms.write, containerPerms.write)
}
