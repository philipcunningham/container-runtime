package nl.rug.digitallab.themis.runtime.container.monitor.monitor

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import jakarta.validation.constraints.Min
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.ByteSizeLongConverter

/**
 * Settings for the process monitor.
 *
 * @property maxWaitForMonitorTerminationMs The maximum amount of time that the process monitor will wait for the container to finish.
 * @property maxPacketSize The maximum size of a packet that can be sent to the gVisor container.
 * @property handshakeTimeoutMs The maximum amount of time that the process monitor will wait for a handshake from gVisor.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.process-monitor")
interface ProcessMonitorConfig {
    @Min(100)
    fun maxWaitForMonitorTerminationMs(): Int
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("max-packet-size")
    fun maxPacketSize(): ByteSize
    @Min(100)
    fun handshakeTimeoutMs(): Int
    @Min(100)
    fun readTimeoutMs(): Int
}
