package nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor

import java.nio.ByteBuffer
import java.nio.ByteOrder

/**
 * A class representing the header of a message sent by a gVisor container.
 *
 * @property headerSize The size of the header in bytes.
 * This has so far always been 8, but it is included in the header for future compatibility.
 * The payload comes directly after the header.
 * @property messageType The type of the message. This value can be used to determine the type of the message body.
 * @property droppedCount DroppedCount is the number of times that an event had to be dropped. It wraps around after max(uint32).
 */
class ProtoHeader(
    val headerSize: Short,
    val messageType: Short,
    val droppedCount: Int,
) {
    companion object {
        /**
         * Creates a ProtoHeader from a byte array.
         *
         * @param byteArray The byte array to create the ProtoHeader from.
         * @return The ProtoHeader created from the byte array.
         */
        fun fromByteArray(byteArray: ByteArray): ProtoHeader {
            // Based on experiments the byte order is little endian.
            // By default, Java uses big endian, so we need to explicitly set the byte order to little endian.
            ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN).let {
                return ProtoHeader(
                    it.short,
                    it.short,
                    it.int,
                )
            }
        }
    }
}
