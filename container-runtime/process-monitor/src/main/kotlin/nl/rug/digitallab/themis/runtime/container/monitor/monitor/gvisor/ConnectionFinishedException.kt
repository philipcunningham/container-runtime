package nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor

import java.io.IOException

/**
 * An exception thrown when the connection to the gVisor container is closed.
 *
 * @param message The message to include in the exception.
 */
class ConnectionFinishedException(
    message: String
): IOException(message)
