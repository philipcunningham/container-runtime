package nl.rug.digitallab.themis.runtime.container.monitor

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.*
import kotlinx.coroutines.test.runTest
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorClient
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorMessage
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.ProtoHeader
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitor
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitorConfig
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.mock
import org.mockito.kotlin.*

/**
 * Tests for the [ProcessMonitor] class.
 */
@QuarkusTest
class ProcessMonitorTest {
    @Inject
    private lateinit var config: ProcessMonitorConfig

    @Test
    fun `Checking whether the process monitor has finished does not infinitely hang`() = runTest {
        withContext(Dispatchers.IO) {
            val monitor = ProcessMonitor(config)

            launch { monitor.monitorContainer(generateMockedClient()) }
            launch {
                // Give the monitoring some time to acquire the lock
                delay(1000)
                assertThrows<TimeoutCancellationException> { monitor.awaitFinish() }
            }
        }
    }

    @Test
    fun `An error produced by the process monitor is passed to the caller`() = runTest {
        val monitor = ProcessMonitor(config)
        val client: GVisorClient = mock()

        whenever(client.handshake()).then {
            throw Exception("Test exception")
        }

        assertDoesNotThrow { monitor.monitorContainer(client) }
        assertThrows<Exception> { monitor.awaitFinish() }
    }

    suspend fun generateMockedClient(): GVisorClient {
        val client: GVisorClient = mock()

        whenever(client.readProtoMessage()).then {
            runBlocking { delay(1000) }
            return@then GVisorMessage(ProtoHeader(0, 2, 0), byteArrayOf())
        }

        return client
    }
}
