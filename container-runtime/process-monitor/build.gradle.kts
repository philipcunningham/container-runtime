val sequencedSocketVersion: String by rootProject
val communicationSpecVersion: String by rootProject

plugins {
    id("org.kordamp.gradle.jandex")
}

dependencies {
    // Digital Lab dependencies
    implementation("nl.rug.digitallab.sequencedsocket:sequenced-socket:$sequencedSocketVersion")

    // Quarkus dependencies
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-grpc")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
