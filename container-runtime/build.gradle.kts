val kotlinMockitoVersion: String by rootProject
val instancioVersion: String by rootProject
val coroutineTestVersion: String by rootProject

subprojects {
    dependencies {
        testImplementation("io.quarkus:quarkus-junit5-mockito")
        testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
        testImplementation("org.instancio:instancio-core:$instancioVersion")
        testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutineTestVersion")
    }

    tasks.named("quarkusDependenciesBuild") {
        dependsOn(":container-runtime:container-runner:jandex", ":container-runtime:process-monitor:jandex")
    }
}
