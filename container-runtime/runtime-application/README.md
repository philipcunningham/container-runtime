# Runtime application
This module is the main entrypoint for the application. It simply connects everything together.
This should be the target when trying to run the application, though it has no serious business logic.
