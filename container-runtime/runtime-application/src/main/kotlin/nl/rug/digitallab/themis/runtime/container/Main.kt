package nl.rug.digitallab.themis.runtime.container

import io.quarkus.runtime.Quarkus
import io.quarkus.runtime.QuarkusApplication
import io.quarkus.runtime.annotations.QuarkusMain
import jakarta.inject.Inject
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.runtime.container.monitor.services.ProcessService

/**
 * The main class for the container runtime.
 */
@QuarkusMain
class Main : QuarkusApplication {
    @Inject
    lateinit var processService: ProcessService

    /**
     * This main method overrides the default Quarkus main method.
     * Its goal is to start custom services before the Quarkus application starts.
     * This currently includes:
     * - The service handling messages received from the gVisor runtime. Used by the process monitor.
     * Quarkus will automatically start:
     * - The service handling gRPC action requests.
     */
    @Throws(Exception::class)
    override fun run(vararg args: String): Int {
        runBlocking {
            async {
                // Start the process-monitor module
                processService.startService()

                // Run the normal Quarkus application. This will include the container-runner module.
                Quarkus.run(*args)
            }
        }

        return 0
    }
}
