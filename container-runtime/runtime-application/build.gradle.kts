dependencies {
    // Submodules
    implementation(project(":container-runtime:process-monitor"))
    implementation(project(":container-runtime:container-runner"))
}
